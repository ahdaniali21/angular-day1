import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { ResponseBase } from "../interfaces/reponse.interface";
import { Users } from "../interfaces/user.interface";

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    private baseApi = 'http://localhost:8080';

    constructor(private httpClient: HttpClient) {}

    listUsers(): Observable<Users[]> {

        // const token = localStorage.getItem('token')
        
        // const headers = new HttpHeaders({
        //     'Authorization': `Bearer ${token}`
        // })

        return this.httpClient.get<ResponseBase<Users[]>>(this.baseApi + '/api/getalluser').pipe(
            map((val) => {
                return val.data;
            }),
            catchError((err) => {
                console.log(err);
                throw err
            })
        )
    }

    // uploadPhoto (data: File) Observeable<ResponseBase<ResponseUploadPhoto>> {
    //     const file = new FormData()
    //     file.append('file', data, data.name)
    //     return this.httpClient.post<ResponseBase<ResponseUploadPhoto>>(`${this.baseApi}/users/uploadPhoto`, file)
    // }

    
}