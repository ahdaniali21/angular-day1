import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { Category } from "../interfaces/category.interface";
import { ResponseBase } from "../interfaces/reponse.interface";

@Injectable({
    providedIn: 'root'
})
export class CategoryService {
    private baseApi = 'http://localhost:8080';

    constructor(private httpClient: HttpClient) {}

    listCategory(): Observable<Category[]> {

        // const token = localStorage.getItem('token')
        
        // const headers = new HttpHeaders({
        //     'Authorization': `Bearer ${token}`
        // })

        return this.httpClient.get<Category[]>(this.baseApi + '/api/kategorisampah/').pipe(
            map((val) => {
                return val;
            }),
            catchError((err) => {
                console.log(err);
                throw err
            })
        )
    }

    addCategory(payload: {
        jenis_sampah: string,
        image: string
    }):Observable<Category[]>{
        return this.httpClient
      .post<ResponseBase<Category[]>>(this.baseApi+'/api/kategorisampah/create/',payload)
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
    }
    

    // uploadPhoto (data: File) Observeable<ResponseBase<ResponseUploadPhoto>> {
    //     const file = new FormData()
    //     file.append('file', data, data.name)
    //     return this.httpClient.post<ResponseBase<ResponseUploadPhoto>>(`${this.baseApi}/users/uploadPhoto`, file)
    // }

    
}