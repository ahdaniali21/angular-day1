import { Component } from '@angular/core';
import { switchMap } from 'rxjs';
import { Users } from 'src/app/interfaces/user.interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { UsersService } from 'src/app/services/users.service';
// import { User } from '../interfaces/user.interface';

@Component({
  selector: 'app-users',  
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  users!: Users[]
  // modalRef!: BsModalRef
  photo!: string
  photoFile!: File

  // users: any
  constructor(private userService: UsersService) {
    this.userService.listUsers().subscribe(
      response => {
        console.log(response)
        this.users = response
      }
    )
  }

  // openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template)
  // }

  // showPreview(event: any) {
  //   if (event) {
  //     constfile = event.target.files[0]
  //     this.photoFile = File
  //     const reader = new FileReader()
  //     reader.onload = () => {
  //       this.photo = reader.result as string:
  //     }
  //     reader.readAsDataURL(file)
  //   }
  // }

  // doAddUser() {
  //   this.userServices.uploadPhoto(this.photoFile).pipe(
  //     switchMap(val => {


  //       return this AuthService.register(

  //       )
  //     })
  //   )
  // }

}