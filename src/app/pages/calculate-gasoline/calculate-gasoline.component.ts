import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {

  bensinForm:FormGroup; 
  hargaBensin = 0;
  Hargaliter = 0;
  Hargabayar = 0;
  total_bayar = 0;
  kembalian = 0;
  jenisBensin = "";
  
  Click(pilihan: any) {
    this.jenisBensin = pilihan
    if (pilihan == "VPower") {
      this.hargaBensin = 20000
    } else if (pilihan == "Super") {
      this.hargaBensin = 15000
    } else if (pilihan == "Nitro") {
      this.hargaBensin = 26000
    } else if (pilihan == "Pertalite") {
      this.hargaBensin = 10000
    } else {
      this.hargaBensin = 6000
    }
    console.log(pilihan)
  }

  constructor(form: FormBuilder) { 
    this.bensinForm = form.group({
      Hargaliter: 0,
      Hargabayar: 0
    });
  }  

  count() {
    this.Hargaliter = this.bensinForm.get('Hargaliter')?.value;
    console.log(this.Hargaliter)
    this.Hargabayar = this.bensinForm.get('Hargabayar')?.value;
    console.log(this.Hargabayar)
    this.total_bayar = this.hargaBensin * this.Hargaliter
    this.kembalian = this.Hargabayar - this.total_bayar
  }

}