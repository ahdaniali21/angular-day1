import { Component } from '@angular/core';
import { Users } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import { LoginComponent } from '../../login/login.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {
  user: Users[] = []
  constructor(private userService: UsersService ) {
    this.userService.listUsers().subscribe(
      response => {
        console.log(response)
        this.user = response
      }
    )
  }
}