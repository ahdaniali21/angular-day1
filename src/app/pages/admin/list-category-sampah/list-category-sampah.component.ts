import { Component, Input, TemplateRef } from '@angular/core';
import { Category } from 'src/app/interfaces/category.interface';
import { CategoryService } from 'src/app/services/category.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { map, switchMap } from 'rxjs';

@Component({
  selector: 'app-list-category-sampah',
  templateUrl: './list-category-sampah.component.html',
  styleUrls: ['./list-category-sampah.component.scss'],
})
export class ListCategorySampahComponent {
  modalRef!: BsModalRef;
  addForm: FormGroup;
  categoryList!: Category[];
  // @Input() jenis_sampah = '';
  // @Input() image = '';

  categoryDetail = {
    id_kategori_sampah: 0, 
    jenis_sampah: '',
    image: ''

  }
  constructor(
    private categoryService: CategoryService,
    private modalService: BsModalService,
    private Sb: FormBuilder
  ) {
    this.categoryService.listCategory().subscribe((response) => {
      console.log(response);
      this.categoryList = response;
    });
    this.addForm = this.Sb.group({
      id_kategori_sampah: [''],
      jenis_sampah: [''],
      image: [''],
    });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  doAddCategory(){
    this.categoryService.listCategory().pipe(
      switchMap((val) => {
        const payload = {
          jenis_sampah: this.addForm.value.jenis_sampah, 
          image: this.addForm.value.image
        }
        return this.categoryService.addCategory(payload).pipe(
          map(val => val)
        )
      })
    ).subscribe(response => {
      Swal.fire({
        title: 'Success!',
        text: 'Anda berhasil menambahkan akun!',
        icon: 'success',
        confirmButtonText: 'OK'
      })
      console.log(response)
    })
  }
  //   const payload={
    // jenis_sampah: this.addForm.value.jenis_sampah, 
    // image: this.addForm.value.image
    
  //   }
  //   this.categoryService.addCategory(payload).subscribe(
  //     res => {
  //       console.log(res)

  //       Swal.fire({
  //         title: 'Success!',
  //         text: 'Ticket has been added',
  //         icon: 'success',
  //         confirmButtonText: 'Ok'
  //       })
  //     }, error => {
  //       console.log(error)
  //       alert(error.error.message)

  //       Swal.fire({
  //         title: 'Failed!',
  //         text: 'Ticket cant be added',
  //         icon: 'error',
  //         confirmButtonText: 'Ok'
  //       })
  //     }
  //   )
  // }
  
}
  
