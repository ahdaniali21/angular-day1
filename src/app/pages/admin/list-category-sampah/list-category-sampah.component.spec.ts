import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategorySampahComponent } from './list-category-sampah.component';

describe('ListCategorySampahComponent', () => {
  let component: ListCategorySampahComponent;
  let fixture: ComponentFixture<ListCategorySampahComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCategorySampahComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListCategorySampahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
