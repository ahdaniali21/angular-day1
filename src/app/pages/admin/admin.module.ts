import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { CategoryComponent } from './product/category/category.component';
import { ListProductComponent } from './product/list-product/list-product.component';
import { UsersComponent } from '../users/users.component';
import { ListUsersComponent } from './product/list-users/list-users.component';
import { ListCategorySampahComponent } from './list-category-sampah/list-category-sampah.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
      {
        path: 'product/category',
        component: CategoryComponent,
      },
      {
        path: 'product/list-product',
        component: ListProductComponent,
      },
      {
        path: 'product/list-user',
        component: UsersComponent,
      },
      {
        path: 'list-category-sampah',
        component: ListCategorySampahComponent,
      },
    ],
  },
];
@NgModule({
  declarations: [AdminComponent, DashboardComponent, SettingsComponent, CategoryComponent, ListProductComponent, UsersComponent, ListUsersComponent, ListCategorySampahComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }