import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as dayjs from 'dayjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  username = '';
  password = '';
  email = '';
  fullname = '';
  tanggal_lahir = '';

  formRegister: FormGroup

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {
    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [Validators.required]],
      fullname: ['', [Validators.required]],
      tanggal_lahir: ['', [Validators.required]],
      c_password: ['', [Validators.required]]

    })
  }
  get errorControl() {
    return this.formRegister.controls
  }

  get confirmPassword(){
    return this.formRegister.value.password == this.formRegister.value.c_password 
  }

  doRegister() {
    const payload = {
      username: this.formRegister.value.username,
      password: this.formRegister.value.password,
      email: this.formRegister.value.email,
      fullname: this.formRegister.value.fullname,
      tanggal_lahir: this.formRegister.value.tanggal_lahir
    }

    this.authService.register(payload).subscribe(
      response => {
        console.log(response)
      }
    )
  }
}