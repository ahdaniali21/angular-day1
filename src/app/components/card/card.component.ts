import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @Input() title = 'angular-day1';

  @Input() name = 'Dani'
  @Input() age = 22
  @Input() status = false

  @Output() dataCallBack = new EventEmitter()

  doClick() {
    this.dataCallBack.emit({ data: { name: this.name , age: this.age, status: this.status}})
    // this.dataCallBack.emit({ data: { age: this.age } })
    // this.dataCallBack.emit({ data: { status: this.status } })
  }
}
