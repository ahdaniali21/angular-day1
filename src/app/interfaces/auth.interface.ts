export interface RequestLogin {
    username: string,
    password: string
  }
  
  export interface ResponseLogin {
    jwttoken: string,
    jwtrole: string
  }
  
  // export interface ResponseRegister {
  //   jwtrole: string;
  // }
  