export interface Users {
  
    id_user: number,
    username: string,
    fullname: string,
    tanggal_lahir: Date,
    email: string,
    admin: boolean
  }