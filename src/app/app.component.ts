import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-day1'; // Deklarasi sebuah variable bertipe string

  // Deklarasi pada typescript, variable dulu baru tipe data. tipe2 data typescript (String, number, boolean, array, unknown, any)

  name = 'Dani'
  age = 22 
  status = false 

  dataArr = [1,2,3]
  dataArrNew = new Array(10)

  person = { 
    title: 'Test A',
    name: 'Dani',
    age: 28, 
    status: true
  }

  personList = [
    {
    title: 'Data 1',
    name: 'Dani',
    age: 22,
    status: true
    },
    {
      title: 'Data 2',
      name: 'Ahdani',
      age: 27,
      status: true 
    }
  ]

  constructor() { 
    this.name = 'Hello, Muhammad Ahdani Himam'
    this.age = 25
  }

  onCallBack(ev: any) {
    console.log(ev);
    this.personList.push(ev.data)
  }
}
